from gluon import dal

CONNECTION_STR = 'sqlite:://databases/99Cells/99cells.db'
PREPAID_CHARGES_TABLE = 'prepaid_charges'
PREPAID_AMOUNT_COL = 'amount'
PREPAID_CHARGES_DATA_CSV_PATH = 'databases/99Cells/prepaid_charges.csv'

def populatePrepaidRechargeDenominations():
    global CONNECTION_STR, PREPAID_CHARGES_TABLE, PREPAID_AMOUNT
    global PREPAID_CHARGES_DATA_CSV_PATH

    db = dal.DAL(uri=CONNECTION_STR,  migrate=True)
    
    field = db.Field(PREPAID_AMOUNT_COL, type='integer', required=True, notnull=True, unique=True)
    
    table = db.define_table(PREPAID_CHARGES_TABLE, field, redefine=False)
    
    charges = db(db.prepaid_charges.amount).select()
    if len(charges) is 0:
        print "First Launch: Need to populate tables..."
        #populatePrepaidChargesInline(db.prepaid_charges)
        # read the charges from csv file
        importPrepaidCharges(db.prepaid_charges, PREPAID_CHARGES_DATA_CSV_PATH)
        db.commit()
    
    showPrepaidCharges(db(db.prepaid_charges.amount).select())
    selectAmountGreaterThan200()   

def populatePrepaidChargesInline(table):
    table.insert(amount=55)
    table.insert(amount=100)
    table.insert(amount=200)
    table.insert(amount=330)
    table.insert(amount=440)
    table.insert(amount=500)
    table.insert(amount=555)
    table.insert(amount=770)
    table.insert(amount=880)
    table.insert(amount=1000)

def importPrepaidCharges(table, csv_path):
    table.import_from_csv_file(open(csv_path, 'r'))

def showPrepaidCharges(prepaid_charges):
    print "prepaid charges:"
    for c in prepaid_charges:
        print '    ', c

def printPrepaidTableDetails(db):
    print db.tables
    print db.prepaid_charges.amount.type

def selectAmountGreaterThan200():
    db = dal.DAL(uri=CONNECTION_STR,  migrate=False)
    field = db.Field(PREPAID_AMOUNT_COL, type='integer', required=True, notnull=True, unique=True)
    table = db.define_table(PREPAID_CHARGES_TABLE, field, redefine=False)
    rowsetAmount = db(table.amount > 100).select(table.amount)
    for row in rowsetAmount:
        print row.amount

if __name__ == '__main__':
    populatePrepaidRechargeDenominations()

